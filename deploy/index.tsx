import Adapt from "@adpt/core";
import { NodeService } from "@adpt/cloud/nodejs";
import { k8sStyle } from "./styles";

function App() {
    return <NodeService srcDir=".." scope="external" />;
}

Adapt.stack("default", <App />, k8sStyle);
Adapt.stack("k8s", <App />, k8sStyle);
