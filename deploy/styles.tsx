import Adapt, { Style } from "@adpt/core";
import { Service, ServiceProps } from "@adpt/cloud";
import { ServiceDeployment } from "@adpt/cloud/k8s";
import * as fs from "fs";

export function kubeconfig() {
    let configPath = process.env.KUBECONFIG;
    if (!configPath) {
        configPath = "./kubeconfig.json";
        if (!fs.existsSync(configPath)) {
            throw new Error(`Cannot find kubeconfig. Environment variable KUBECONFIG not set and ${configPath} not found`);
        }
    }
    return {
        kubeconfig: require(configPath)
    }
}

/*
 * Kubernetes testing style
 */
export const k8sStyle =
    <Style>
        {Service}
        {Adapt.rule((matchedProps) => {
            const { handle, ...remainingProps } = matchedProps;
            return <ServiceDeployment config={kubeconfig()} {...remainingProps} />;
        })}
    </Style>;

